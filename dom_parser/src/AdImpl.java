import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

public class AdImpl implements IAd {
    @Override
    public List<Ad> getAll() throws ParserConfigurationException, IOException, SAXException {
        List<Ad> result = new ArrayList<>();

        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);

        System.out.println("Root element is " + document.getDocumentElement().getTagName());

        NodeList nodeList = document.getElementsByTagName("ad");

        Ad currentAd;
        for(int i = 0; i < nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            currentAd = new Ad();

            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element)node;
                currentAd.setTitle(element.getElementsByTagName("title").item(0).getTextContent());              
                currentAd.setAuthor(element.getElementsByTagName("author").item(0).getTextContent());
                currentAd.setContent(element.getElementsByTagName("content").item(0).getTextContent());
            }

            result.add(currentAd);
        }

        return result;
    }

    public Optional<Ad> getByTitle(String title) throws IOException, SAXException, ParserConfigurationException {
        Optional<Ad> ad = this.getAll().stream()
                .filter(x -> x.getTitle().equals(title))
                .findFirst();
        return ad;
    }

    @Override
    public Ad add(Ad ad) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);
        Element root = document.getDocumentElement();

        Element adTag = document.createElement("ad");

        Element adTitle = document.createElement("title");
        adTitle.appendChild(document.createTextNode(ad.getTitle()));
        adTag.appendChild(adTitle);

        Element adAuthor = document.createElement("author");
        adAuthor.appendChild(document.createTextNode(ad.getAuthor()));
        adTag.appendChild(adAuthor);

        Element adContent = document.createElement("content");
        adContent.appendChild(document.createTextNode(ad.getContent()));
        adTag.appendChild(adContent);

        root.appendChild(adTag);

        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("file.xml");
        transformer.transform(source, result);

        return null;
    }

    @Override
    public void deleteByTitle(String titleT) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);

        NodeList nodes = document.getElementsByTagName("ad");
     
        for (int i = 0; i < nodes.getLength(); i++) {
        Element ad = (Element)nodes.item(i);
      
        Element title = (Element)ad.getElementsByTagName("title").item(0);
        String target = title.getTextContent();
        
         if (target.equals(titleT)) { 
          ad.getParentNode().removeChild(ad);
         }
        }
        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("file.xml");
        transformer.transform(source, result);
    }

    @Override
    public void updateByTitle(String titleT, String newTitle, String newAuthor, String newContent) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);

        NodeList nodes = document.getElementsByTagName("ad");
     
        for (int i = 0; i < nodes.getLength(); i++) {
        Element ad = (Element)nodes.item(i);
      
        Element title = (Element)ad.getElementsByTagName("title").item(0);
        String target = title.getTextContent();
        
         if (target.equals(titleT)) { 
          title.setTextContent(newTitle);

          Element author = (Element)ad.getElementsByTagName("author").item(0);
          author.setTextContent(newAuthor);
 
          Element content = (Element)ad.getElementsByTagName("content").item(0);
          content.setTextContent(newContent); 
         }
        }
        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("file.xml");
        transformer.transform(source, result);
    }
    
    
    public void updateByTitle(String titleT, String newTitle, String newAuthor) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);

        NodeList nodes = document.getElementsByTagName("ad");
     
        for (int i = 0; i < nodes.getLength(); i++) {
        Element ad = (Element)nodes.item(i);
      
        Element title = (Element)ad.getElementsByTagName("title").item(0);
        String target = title.getTextContent();
        
         if (target.equals(titleT)) { 
          title.setTextContent(newTitle);

          Element author = (Element)ad.getElementsByTagName("author").item(0);
          author.setTextContent(newAuthor); 
         }
        }
        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("file.xml");
        transformer.transform(source, result);
    }

    public void updateByTitle(String titleT, String newTitle) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        File xmlFile = new File("file.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);

        NodeList nodes = document.getElementsByTagName("ad");
     
        for (int i = 0; i < nodes.getLength(); i++) {
        Element ad = (Element)nodes.item(i);
      
        Element title = (Element)ad.getElementsByTagName("title").item(0);
        String target = title.getTextContent();
        
         if (target.equals(titleT)) { 
          title.setTextContent(newTitle);
         }
        }
        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("file.xml");
        transformer.transform(source, result);
    }
}
