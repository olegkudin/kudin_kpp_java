import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;

class Main {
    public static void main(String[] args){
        AdImpl adobj = new AdImpl();
        try {
            List<Ad> ads =  adobj.getAll(); 
            ads.stream()
                   .forEach(System.out::println);
            adobj.add(new Ad("garage", "Anton", "prodam garage"));
            adobj.getByTitle("Car").ifPresent(System.out::println);         
            adobj.add(new Ad("bycicle", "Ivan", "prodam bycicle"));
            adobj.deleteByTitle("bycicle"); 
            adobj.updateByTitle("Car", "Car", "Anton", "prodam car"); 
            adobj.updateByTitle("bycicle2", "bycicle2", "Anton2"); 
            adobj.updateByTitle("garage", "NEWgarage"); 
            adobj.getAll();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}