import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IAd {
    List<Ad> getAll() throws ParserConfigurationException, IOException, SAXException;
    Optional<Ad> getByTitle(String title)  throws IOException, SAXException, ParserConfigurationException;
    Ad add(Ad ad) throws ParserConfigurationException, IOException, SAXException, TransformerException;
    void deleteByTitle(String titleT) throws ParserConfigurationException, IOException, SAXException, TransformerException;
    void updateByTitle(String titleT, String newTitle, String newAuthor, String newContent) throws ParserConfigurationException, IOException, SAXException, TransformerException;
}