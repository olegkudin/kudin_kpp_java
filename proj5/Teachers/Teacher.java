package Teachers;

import Persons.*;

public class Teacher extends Person{

   String specialty;
   String subjects;

   public Teacher(String n, String s, int a, String p, String spec, String sub){
 
      super(n, s, a, p);
      this.specialty = spec;
      this.subjects = sub; 
   }

   public void showTeacher(){

      System.out.println("");
      super.showPerson();
      System.out.print(" Speciality: " + specialty + " Subjects:" + subjects);
   }
}