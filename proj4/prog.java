import ArrayWork.*;

class MainClass {

 public static void main(String args[]) {
  
 ArrayClass obj = new ArrayClass();
 obj.genArray();
 System.out.println("Min value of array: "+ obj.minValue());
 System.out.println("Count of negative values: "+ obj.negCount());
 }

}